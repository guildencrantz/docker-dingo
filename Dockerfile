FROM scratch

ENTRYPOINT [ "/dingo" ]

# dingo's default port
EXPOSE 32000/udp

ADD ca-certificates.crt /etc/ssl/certs/

ADD dingo /

