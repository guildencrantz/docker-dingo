DINGO_VERSION = 0.13

dingo:
	finally() {                                                          \
	  docker cp dingo-build:/go/src/github.com/pforemski/dingo/dingo . ; \
	  docker rm dingo-build 2>/dev/null;                                 \
	};                                                                   \
	trap finally EXIT HUP INT QUIT KILL TERM ;                           \
	docker run                                                           \
	  --name dingo-build                                                 \
	  --entrypoint bash                                                  \
	  guildencrantz/golang-glide:1.8-0.12                                \
	  -c '                                                               \
	    set -x;                                                          \
	    go get github.com/pforemski/dingo            &&                  \
	    cd $${GOPATH}/src/github.com/pforemski/dingo &&                  \
	    git checkout $(DINGO_VERSION)                &&                  \
	    CGO_ENABLED=0 GOOS=linux go build                                \
	  '

ca-certificates.crt:
	curl -sL https://curl.haxx.se/ca/cacert-2017-01-18.pem > ca-certificates.crt;
	echo "38cd779c9429ab6e2e5ae3437b763238  ca-certificates.crt" | md5sum -c -; \
	if [ $$? -ne 0 ]; then                                                      \
	  rm -f ca-certificates.crt && false;                                       \
	fi;

.packaged: dingo ca-certificates.crt Dockerfile
	docker build -t guildencrantz/dingo:$(DINGO_VERSION) . | tee .packaged; \
	if [[ $$? -ne 0 ]]; then                                                \
	  rm -f .packaged && false;                                             \
	fi
	docker tag guildencrantz/dingo:$(DINGO_VERSION) guildencrantz/dingo:latest

.PHONY: package
package: .packaged

.PHONY: build
build: package

.shipped: .packaged
	docker push guildencrantz/dingo:$(DINGO_VERSION)
	docker push guildencrantz/dingo:latest

.PHONY: ship
ship: .shipped

.PHONY: push
push: ship

clean:
	rm -f .packaged
	rm -f .shipped
	rm -f dingo
