# About

This just builds a docker image with the [dingo](https://github.com/pforemski/dingo) binary (pure-go, no cgo) and ca-certificates, setting dingo as the entrypoint.

# Install

You can download and run `make package` to do everything locally, but the easiest thing is to just use [`guildencrantz/dingo`](https://hub.docker.com/r/guildencrantz/dingo/).

# Usage

    docker run -d --name dingo -p 32000:32000/udp guildencrantz/dingo -bind 0.0.0.0 -gdns:auto

This uses the dingo default port of 32000. If you're not going to put [dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html), or another caching layer, in front of dingo you change the `32000`s to `53`s and append `-port 53` to have it directly handle DNS lookups.

*NOTE:* by default dingo binds to `127.0.0.1`, so to make the port reachable through the docker network you have to add `-bind` to make the port available to the docker network. The instructions above don't exactly mimic dingo's default behavior (locking it down to the local machine only), but you can do that by changing the port mapping to `-p 127.0.0.1:32000:32000/udp`.
